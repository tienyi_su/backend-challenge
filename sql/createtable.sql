CREATE TABLE IF NOT EXISTS suburb (
    id int NOT NULL,
    postcode int NOT NULL,
    locality text NOT NULL,
    state text NOT NULL,
    comments text,
    category text NOT NULL,
    coordinates point NOT NULL,
    PRIMARY KEY (id)
);

CREATE SPATIAL INDEX suburb_coordinates ON suburb(coordinates);
CREATE INDEX suburb_locations ON suburb (locality(30), postcode);