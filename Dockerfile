FROM amazoncorretto:8u212
COPY ./target/backend-challenge-1.0-SNAPSHOT-spring-boot.jar /usr/app/
COPY ./wait-for-it.sh /usr/app
WORKDIR /usr/app
# needs to be executable
RUN chmod +x wait-for-it.sh
# wait script is added here so the MySQL container is started and data is loaded before the Spring Boot container spins up
# this prevents connection errors from the Spring Boot app
ENTRYPOINT ["./wait-for-it.sh", "challenge-mysql:3306", "--timeout=45", "--", "java", "-jar", "backend-challenge-1.0-SNAPSHOT-spring-boot.jar"]