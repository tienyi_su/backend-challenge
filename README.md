# Backend-Challenge

A solution to NurtureCloud's take home challenge.

Users can enter an Australian suburb and postcode and get nearby and fringe suburbs in return. 
Nearby suburbs are suburbs within 10km and fringe suburbs are suburbs within 50km of the set 
origin suburb (user input).

## Getting Started

### Prerequisites
1. You'll need Docker to spin up the app with its MySQL database. I used Docker 20.10.2. Earlier Docker
versions may have issues. You will also need Docker Compose which should already be installed with Docker. My Docker
Compose version is 1.27.4.
2. You will need Java 8. I used Amazon Corretto 1.8 but any JDK should work.
3. Last but not least, you will need Apache Maven. I used 3.6.3.

That is all!

### Let's Go!
We will be using Docker Compose to spin up the app and MySQL containers. Execute the following
commands in your terminal of choice (I used zsh). Make sure Maven and Docker have been added to 
your PATH. All commands from here on out will be run from the root directory.

1. So first we need to build the fat JAR using Maven. Skippable if it already exists in the 
`target` folder.  
Note: Currently `mvn test` doesn't work because of incompatibilities with 
Spring Boot 2.x and JUnit 4.

    ```
    mvn clean package -DskipTests
    ```
   
2. Now we will build the images with `docker-compose`.

    ```
    docker-compose build
    ```
   
3. Finally we will use `docker-compose` to spin up the containers using the built images.   
Note: `docker-compose up` cannot be used here because we need an interactive shell. Also
don't run it in detached mode for the same reason.

   ```
   docker-compose run challenge-app
   ```
   
4. That's it! You should see a prompt asking you to enter a suburb name and postcode after the app has
been started. Enter a suburb name here (case insensitive) such as "Sydney" and a postcode such as "2000" and
see the results appear in the shell.  
When you're done, exit the app using `Ctrl+C` and run the following command
to shut down the containers:

   ```
   docker-compose down
   ```
   
### Wait, what's going on here?
1. The input mechanism (Scanner) lives in `InputRunner`. This class implements `CommandLineRunner` which has a 
`run` method that is invoked after the Spring context has been loaded. This allows it to have dependencies
injected into it.
2. After the input has been entered, `InputRunner` calls `SuburbService` which has `SuburbRepository` as
a dependency. This class is responsible for calling the repository to get the results.
3. `SuburbService` will first call the DB to get the suburb that matches the user input. This initial call 
will be cached. Next it gets the coordinates (longitude and latitude) and calls the DB again to calculate
the nearest and fringe suburbs. 
4. `SuburbRepository` now does its magic. It uses a combination of the Haversine formula and MBRContains to
quickly return a set of results.
5. These results come in the form of a `SuburbResult` interface. JPA uses interface projection to map the
results of the `@Query` here. Unfortunately it cannot be converted to a concrete object at this time
because of a Spring bug. Have no fear, however, because a concrete object is implemented in the unit test for
assertions.
6. `SuburbService` now returns these results back to `InputRunner` which runs through the results and displays
only the suburb name and postcode for each result in the shell.

Viola!


## Notes and Interesting Things
- As mentioned before, Spring Boot 2.x and JUnit 4 seem to have incompatibilities. Possibly
something needs to be configured on the `maven-surefire-plugin`.
- There are some simple unit and integration tests. The integration test uses `Testcontainers`
and will spin up a real MySQL container as its data source. See Javadoc for more info.
- Results are cached using Spring `@Cacheable`.
- The first time the DB is hit when the containers are freshly spun up can be a little slow.
Subsequent (uncached) hits are much faster.
- SQL insert script was generated outside of this project from the provided `JSON` file.
- There is a small wait script included in the Dockerfile. This is necessary because
we need to wait for MySQL to spin up before the Spring Boot app can be started otherwise
we will get connection errors.
- The Haversine formula is used together with MBRContains to get the results very quickly. MBRContains
will create a rectangle bounded by coordinates to cut down on the rows that need to have their distance
from the origin calculated. All the heavy lifting here is done by MySQL.

## Limitations and Trade-offs
- As all the heavy lifting is done by MySQL and requires spatial types and indexes as well as MBRContains,
this app is therefore tightly coupled to MySQL. Any move to another database type will have to consider
these requirements. Also, it is possibly also tightly coupled to MySQL 8.0.23 (the version currently being used).
- The cache is currently stored in memory so it gets destroyed if the app is shut down. Better to have a separate 
Redis container or something similar.
- A weird combination of Spring Boot 2 and JUnit 4 is breaking things like `mvn test`.
- As the MySQL container is running on the same machine as the Java app, the CPU load is still on the host machine.
In production, it would be more ideal to have MySQL live on a remote DB like RDS that can be easily scaled 
separately from the Java app.
- It also has to insert the data set every time it is spun up which adds to the startup time. Again, this issue
will disappear with something like RDS.

## Tech Stack
- Java 8
- Spring Boot 2.4.0 with Spring Data JPA
- MySQL
- Testcontainers for integration testing
- Docker

## References
- https://stackoverflow.com/questions/1006654/fastest-way-to-find-distance-between-two-lat-long-points