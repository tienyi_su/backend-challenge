package com.nurturecloud.models;

/**
 * Result class that relies on interface projection.
 * Cannot be converted to a concrete class because of https://github.com/spring-projects/spring-data-jpa/issues/2009
 */
public interface SuburbResult {

  int getPostcode();
  void setPostcode(int postcode);

  String getLocality();
  void setLocality(String locality);

  double getDistance();
  void setDistance(double distance);
}
