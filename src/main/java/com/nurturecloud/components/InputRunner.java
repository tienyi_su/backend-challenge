package com.nurturecloud.components;

import com.nurturecloud.models.SuburbResult;
import com.nurturecloud.services.SuburbService;
import java.util.List;
import java.util.Scanner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Class that implements CommandLineRunner to get the command line inputs.
 * Code was moved here from the main method because the run() method here runs after the Spring context has been loaded
 * which allows the service class to be injected.
 */
@Component
public class InputRunner implements CommandLineRunner {

  private final SuburbService service;

  public InputRunner(SuburbService service) {
    this.service = service;
  }

  @Override
  public void run(String... args) {
    Scanner command = new Scanner(System.in);
    boolean running = true;

    while (running) {

      System.out.print("Please enter a suburb name: ");
      String suburbName = command.nextLine();

      System.out.print("Please enter the postcode: ");
      String postcode = command.nextLine();

      List<SuburbResult> nearbySuburbs = service.getNearbySuburbs(suburbName, postcode);
      List<SuburbResult> fringeSuburbs = service.getFringeSuburbs(suburbName, postcode);

      if (nearbySuburbs.isEmpty() && fringeSuburbs.isEmpty()) {
        System.out.printf("\nNothing found for %s, %s!!\n%n", suburbName, postcode);
      } else {
        System.out.print("\nNearby Suburbs: \n\n");

        for (SuburbResult result : nearbySuburbs) {
          System.out.printf("%s %s\n", result.getLocality(), result.getPostcode());
        }

        System.out.print("\nFringe Suburbs: \n\n");

        for (SuburbResult result : fringeSuburbs) {
          System.out.printf("%s %s\n", result.getLocality(), result.getPostcode());
        }

        System.out.println();
      }
    }

    command.close();
  }
}
