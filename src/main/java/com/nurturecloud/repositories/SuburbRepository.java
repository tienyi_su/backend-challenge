package com.nurturecloud.repositories;

import com.nurturecloud.entities.SuburbEntity;
import com.nurturecloud.models.SuburbResult;
import java.util.List;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository for the suburb table.
 * Tightly coupled with MySQL due to the MBRContains query (or any other DB that supports MBRContains and spatial data types).
 *
 * Results are cached but currently not evicted.
 */
public interface SuburbRepository extends JpaRepository<SuburbEntity, Integer> {

  String HAVERSINE_FORMULA = "(6371 * ACOS(COS(RADIANS(:lat)) * COS(RADIANS(ST_Y(coordinates))) "
      + "* COS(RADIANS(ST_X(coordinates)) - RADIANS(:lon)) + SIN(RADIANS(:lat)) "
      + "* SIN(RADIANS(ST_Y(coordinates))))) AS distance";

  String MBR_CONTAINS = "MBRContains "
      + "(LineString(Point (:lon + 15 / (111.320 * COS(RADIANS(:lat))), :lat + 15 / 111.133), "
      + "Point (:lon - 15 / (111.320 * COS(RADIANS(:lat))), :lat - 15 / 111.133)), coordinates)";

  /**
   * Query to find suburbs by distance in KM (min and max). Uses the Haversine formula to calculate the distance from the origin suburb
   * and MBRContains to limit the search radius.
   *
   * @param longitude Longitude of the origin suburb (X coordinate).
   * @param latitude Latitude of the origin suburb (Y coordinate).
   * @param minDistance Minimum maxDistance to search in KM.
   * @param maxDistance Maximum maxDistance to search in KM.
   * @return list of suburbs matching the search criteria.
   */
  @Query(value = "SELECT s.locality, s.postcode, " + HAVERSINE_FORMULA
      + " FROM suburb s"
      + " WHERE " + MBR_CONTAINS
      + " HAVING distance > :minDistance AND distance <= :maxDistance"
      + " ORDER BY distance"
      + " LIMIT 15", nativeQuery = true)
  @Cacheable(value = "suburbsByDistance")
  List<SuburbResult> findSuburbsWithinDistance(@Param("lon") double longitude, @Param("lat") double latitude,
      @Param("minDistance") double minDistance, @Param("maxDistance") double maxDistance);

  @Cacheable(value = "suburbByLocalityAndPostcode")
  SuburbEntity findSuburbEntityByLocalityAndPostcode(String locality, int postcode);
}
