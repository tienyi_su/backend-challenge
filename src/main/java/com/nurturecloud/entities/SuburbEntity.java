package com.nurturecloud.entities;

import com.vividsolutions.jts.geom.Point;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "suburb")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SuburbEntity {

  @Id
  private int id;

  @Column(name = "postcode", nullable = false)
  private int postcode;

  @Column(name = "locality", nullable = false)
  private String locality;

  @Column(name = "state", nullable = false)
  private String state;

  @Column(name = "comments")
  private String comments;

  @Column(name = "category", nullable = false)
  private String category;

  @Column(name = "coordinates", nullable = false, columnDefinition = "POINT")
  private Point coordinates;
}
