package com.nurturecloud.services;

import com.nurturecloud.entities.SuburbEntity;
import com.nurturecloud.models.SuburbResult;
import com.nurturecloud.repositories.SuburbRepository;
import com.vividsolutions.jts.geom.Point;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * This service class uses the {@link SuburbRepository} to retrieve both the nearby (<= 10km) and fringe suburbs (<= 50km).
 */
@Service
public class SuburbService {

  private final SuburbRepository repository;

  public SuburbService(SuburbRepository repository) {
    this.repository = repository;
  }

  public List<SuburbResult> getNearbySuburbs(String suburb, String postcode) {
    return getSuburbsByDistance(suburb, Integer.parseInt(postcode), 0.1d,10.0d);
  }

  public List<SuburbResult> getFringeSuburbs(String suburb, String postcode) {
    return getSuburbsByDistance(suburb, Integer.parseInt(postcode), 10.0d, 50.0d);
  }

  private List<SuburbResult> getSuburbsByDistance(String suburbName, int postcode, double minDistance, double maxDistance) {
    // this can technically be done in a single DB call with a SELECT INTO
    // but Spring Data JPA doesn't seem to support multiple queries in a single method.
    SuburbEntity suburb = repository.findSuburbEntityByLocalityAndPostcode(suburbName.toUpperCase(), postcode);

    if (suburb == null) {
      return new ArrayList<>();
    }

    Point coordinates = suburb.getCoordinates();

    return repository.findSuburbsWithinDistance(coordinates.getX(), coordinates.getY(), minDistance, maxDistance);
  }
}
