package com.nurturecloud.repositories;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import com.nurturecloud.entities.SuburbEntity;
import com.nurturecloud.models.SuburbResult;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import java.util.List;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.MySQLContainer;

/**
 * Integration test against a real DB for the {@link SuburbRepository}. A real MySQL DB is needed here because
 * H2 doesn't support MBRContains.
 *
 * Testcontainers is used to spin up a real Docker MySQL container to run the tests against.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SuburbRepositoryIntegrationTest {

  @ClassRule
  public static MySQLContainer mysqlContainer = new MySQLContainer("mysql");

  @Autowired
  private SuburbRepository repository;

  @Before
  public void setup() {
    insertSuburbs();
  }

  @Test
  public void findSuburbsByLocalityAndPostcode_should_return_result(){
    SuburbEntity result = repository.findSuburbEntityByLocalityAndPostcode("BARANGAROO", 2000);

    assertThat(result.getLocality(), is("BARANGAROO"));
    assertThat(result.getId(), is(1));
    assertThat(result.getPostcode(), is(2000));
    assertThat(result.getState(), is("NSW"));
    assertThat(result.getCoordinates().getX(), is(151.2015));
    assertThat(result.getCoordinates().getY(), is(-33.8614));
  }

  @Test
  public void findSuburbsWithinDistance_within50km_expectFringeSuburb(){
    List<SuburbResult> results = repository.findSuburbsWithinDistance(151.2015, -33.8614, 10.0d, 50.0d);

    assertThat(results, hasSize(1));
    assertThat(results.get(0).getLocality(), is("LUGARNO"));
    assertThat(results.get(0).getPostcode(), is(2210));
  }

  private void insertSuburbs() {
    GeometryFactory factory = new GeometryFactory();

    SuburbEntity barangaroo = SuburbEntity.builder()
        .id(1)
        .postcode(2000)
        .locality("BARANGAROO")
        .category("Delivery Area")
        .comments(null)
        .state("NSW")
        .coordinates(factory.createPoint(new Coordinate(151.2015, -33.8614)))
        .build();

    SuburbEntity lugarno = SuburbEntity.builder()
        .id(2)
        .postcode(2210)
        .locality("LUGARNO")
        .category("Delivery Area")
        .comments(null)
        .state("NSW")
        .coordinates(factory.createPoint(new Coordinate(151.0454, -33.9814)))
        .build();

    SuburbEntity bendigo = SuburbEntity.builder()
        .id(3)
        .postcode(3550)
        .locality("BENDIGO")
        .category("Delivery Area")
        .comments(null)
        .state("VIC")
        .coordinates(factory.createPoint(new Coordinate(144.2809, -36.7561)))
        .build();

    repository.save(barangaroo);
    repository.save(lugarno);
    repository.save(bendigo);
    repository.flush();
  }
}
