package com.nurturecloud.services;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nurturecloud.entities.SuburbEntity;
import com.nurturecloud.models.SuburbResult;
import com.nurturecloud.repositories.SuburbRepository;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SuburbServiceTest {

  @Mock
  private SuburbRepository repository;

  private SuburbService service;

  private final double LONGITUDE = 151.2099d;
  private final double LATITUDE = -33.8697d;
  private final double MIN_DISTANCE_NEARBY = 0.1d;
  private final double MIN_DISTANCE_FRINGE = 10d;
  private final double MAX_DISTANCE_NEARBY = 10d;
  private final double MAX_DISTANCE_FRINGE = 50d;
  private final String SYDNEY_LOCALITY = "SYDNEY";
  private final int SYDNEY_POSTCODE = 2000;
  private final int INVALID_POSTCODE = 2118;

  @Before
  public void setup() {
    service = new SuburbService(repository);
  }

  @Test
  public void getNearbySuburbs_validSuburbAndPostcode_expectListOfSuburbs() {
    SuburbEntity suburbEntity = createSuburbEntity();

    SuburbResult result = createSuburbResult("WOOLLOOMOOLOO", 2011, 1.0d);

    when(repository.findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, SYDNEY_POSTCODE)).thenReturn(suburbEntity);
    when(repository.findSuburbsWithinDistance(LONGITUDE, LATITUDE, MIN_DISTANCE_NEARBY, MAX_DISTANCE_NEARBY)).thenReturn(singletonList(result));

    List<SuburbResult> results = service.getNearbySuburbs(SYDNEY_LOCALITY, Integer.toString(SYDNEY_POSTCODE));

    verify(repository).findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, SYDNEY_POSTCODE);
    verify(repository).findSuburbsWithinDistance(LONGITUDE, LATITUDE, MIN_DISTANCE_NEARBY, MAX_DISTANCE_NEARBY);

    assertThat(results, hasSize(1));
    assertThat(results.get(0).getLocality(), is("WOOLLOOMOOLOO"));
    assertThat(results.get(0).getPostcode(), is(2011));
  }

  @Test
  public void getFringeSuburbs_validSuburbAndPostcode_expectListOfSuburbs() {
    SuburbEntity suburbEntity = createSuburbEntity();

    SuburbResult result = createSuburbResult("NORTH BALGOWLAH", 2093, 15.0d);

    when(repository.findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, SYDNEY_POSTCODE)).thenReturn(suburbEntity);
    when(repository.findSuburbsWithinDistance(LONGITUDE, LATITUDE, MIN_DISTANCE_FRINGE, MAX_DISTANCE_FRINGE)).thenReturn(singletonList(result));

    List<SuburbResult> results = service.getFringeSuburbs(SYDNEY_LOCALITY, Integer.toString(SYDNEY_POSTCODE));

    verify(repository).findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, SYDNEY_POSTCODE);
    verify(repository).findSuburbsWithinDistance(LONGITUDE, LATITUDE, MIN_DISTANCE_FRINGE, MAX_DISTANCE_FRINGE);

    assertThat(results, hasSize(1));
    assertThat(results.get(0).getLocality(), is("NORTH BALGOWLAH"));
    assertThat(results.get(0).getPostcode(), is(2093));
  }

  @Test
  public void getNearbySuburbs_invalidSuburbAndPostcode_expectEmptyListOfSuburbs() {
    when(repository.findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, INVALID_POSTCODE)).thenReturn(null);

    List<SuburbResult> results = service.getNearbySuburbs(SYDNEY_LOCALITY, Integer.toString(INVALID_POSTCODE));

    verify(repository).findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, INVALID_POSTCODE);

    assertTrue(results.isEmpty());
  }

  @Test
  public void getFringeSuburbs_invalidSuburbAndPostcode_expectEmptyListOfSuburbs() {
    when(repository.findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, INVALID_POSTCODE)).thenReturn(null);

    List<SuburbResult> results = service.getFringeSuburbs(SYDNEY_LOCALITY, Integer.toString(INVALID_POSTCODE));

    verify(repository).findSuburbEntityByLocalityAndPostcode(SYDNEY_LOCALITY, INVALID_POSTCODE);

    assertTrue(results.isEmpty());
  }

  private SuburbResult createSuburbResult(String locality, int postcode, double distance) {
    return SuburbResultImpl.builder()
        .locality(locality)
        .postcode(postcode)
        .distance(distance)
        .build();
  }

  private SuburbEntity createSuburbEntity() {
    GeometryFactory factory = new GeometryFactory();

    return SuburbEntity.builder()
        .id(1)
        .postcode(2000)
        .locality("SYDNEY")
        .category("Delivery Area")
        .comments(null)
        .state("NSW")
        .coordinates(factory.createPoint(new Coordinate(151.2099, -33.8697)))
        .build();
  }
}

@Getter
@Setter
@Builder
@AllArgsConstructor
class SuburbResultImpl implements SuburbResult {

  private int postcode;
  private String locality;
  private double distance;
}
